package com.cch.platform.test;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.cch.platform.file.util.ExcelReader;

public class FileUtilTest {

	@Test
	public void excelReaderTest() {
		InputStream source=FileUtilTest.class.getResourceAsStream("/record.xlsx");
		ExcelReader reader=new ExcelReader();
		List<Map<String,Object>> data=reader.readExcel(source);
		for(Map<String,Object> rowMap:data){
			for(Map.Entry<String, Object> entry:rowMap.entrySet()){
				System.out.print(entry.getKey()+":"+entry.getValue()+"  ");
			}
			System.out.println("");
		}
	}

}
