package com.cch.platform.core.web;

import java.util.Map;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cch.platform.core.service.FlexsetService;


@Controller
@RequestMapping(value = "/core/FlexsetController/")
public class FlexsetController {

	@Autowired
	FlexsetService fs=null;
	
	@RequestMapping(value = "queryByCode.do")
	public String queryByCode(ServletRequest request,ModelMap mm) {
		Map<String, Object> re=fs.getFlexsetValues(request.getParameter("FLEXSET_CODES"));
		mm.putAll(re);
		return "jsonView";
	}
	
}
