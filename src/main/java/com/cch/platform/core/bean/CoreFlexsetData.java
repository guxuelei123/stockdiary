package com.cch.platform.core.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * CoreFlexsetData entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "core_flexset_data")
public class CoreFlexsetData implements java.io.Serializable {

	// Fields

	private Integer dataId;
	private String flexsetCode;
	private String code;
	private String name;
	private String pcode;
	private Integer sort;

	// Constructors

	/** default constructor */
	public CoreFlexsetData() {
	}

	/** full constructor */
	public CoreFlexsetData(String flexsetCode, String code, String name,
			String pcode, Integer sort) {
		this.flexsetCode = flexsetCode;
		this.code = code;
		this.name = name;
		this.pcode = pcode;
		this.sort = sort;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "data_id", unique = true, nullable = false)
	public Integer getDataId() {
		return this.dataId;
	}

	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}

	@Column(name = "flexset_code", length = 100)
	public String getFlexsetCode() {
		return this.flexsetCode;
	}

	public void setFlexsetCode(String flexsetCode) {
		this.flexsetCode = flexsetCode;
	}

	@Column(name = "code", length = 100)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "name", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "pcode", length = 100)
	public String getPcode() {
		return this.pcode;
	}

	public void setPcode(String pcode) {
		this.pcode = pcode;
	}

	@Column(name = "sort")
	public Integer getSort() {
		return this.sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

}