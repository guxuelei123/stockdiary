package com.cch.stock.bill.web;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cch.platform.file.util.ExcelReader;
import com.cch.platform.web.WebUtil;
import com.cch.stock.bill.bean.SdBill;
import com.cch.stock.bill.service.BillService;

@Controller
@RequestMapping(value="/bill/bstransfer")
public class BstransferController {

	@Autowired
	private BillService service;
	
	@RequestMapping(value = "/pagequery.do")
	public ModelAndView  pageQuery(ServletRequest request) {
		Map<String, Object> param=WebUtil.getParam(request);
		param.put("userId", WebUtil.getUser().getUserId());
		param.put("billBigcatory", "BSTRANS");
		return new ModelAndView("jsonView",service.pageQuery(param));
	}
	
	@RequestMapping(value = "/save.do")
	public ModelAndView save(SdBill bill, ServletRequest request) throws Exception {
		bill.setUserId(WebUtil.getUser().getUserId());
		service.saveOrUpdate(bill);
		return WebUtil.sucesseView("保存成功！");
	}
	
	@RequestMapping(value = "/delete.do")
	public ModelAndView delete(SdBill bill,ServletRequest request) {
		service.delete(bill);
		return WebUtil.sucesseView("删除成功！");
	}
	
	@RequestMapping(value = "/upload.do")
	public ModelAndView upload(MultipartFile file, ServletRequest request) throws Exception {
		if (!file.isEmpty()) {
			ExcelReader reader = new ExcelReader();
			List<Map<String, Object>> data = reader.readExcel(file.getInputStream());
			service.importBstransfer(data);
		}
		return WebUtil.sucesseView("导入成功！");
	}
}
