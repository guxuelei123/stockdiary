package com.cch.stock.bill.web;

import java.util.Map;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cch.platform.web.WebUtil;
import com.cch.stock.bill.bean.SdBill;
import com.cch.stock.bill.service.BillService;

@Controller
@RequestMapping(value="/bill/billcontroller")
public class BillController {

	@Autowired
	private BillService service;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/pagequery.do")
	public ModelAndView  pageQuery(ServletRequest request) {
		Map<String, Object> param=WebUtil.getParam(request);
		param.put("billBigcatory", "BSTRANS");
		return new ModelAndView("jsonView",service.pageQuery(param));
	}
	
	@RequestMapping(value = "/save.do")
	public ModelAndView save(SdBill bill, ServletRequest request) throws Exception {
		service.saveOrUpdate(bill);
		return WebUtil.sucesseView("保存成功！");
	}
	
	@RequestMapping(value = "/delete.do")
	public ModelAndView delete(SdBill bill,ServletRequest request) {
		service.delete(bill);
		return WebUtil.sucesseView("保存成功！");
	}
}
