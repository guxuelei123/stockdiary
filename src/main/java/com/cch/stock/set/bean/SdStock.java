package com.cch.stock.set.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * SdStock entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sd_stock")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SdStock implements java.io.Serializable {

	// Fields

	private String stockCode;
	private String stockName;
	private String stockType;
	private String stockMarket;

	// Constructors

	/** default constructor */
	public SdStock() {
	}

	/** minimal constructor */
	public SdStock(String stockCode) {
		this.stockCode = stockCode;
	}

	/** full constructor */
	public SdStock(String stockCode, String stockName, String stockType,
			String stockMarket) {
		this.stockCode = stockCode;
		this.stockName = stockName;
		this.stockType = stockType;
		this.stockMarket = stockMarket;
	}

	// Property accessors
	@Id
	@Column(name = "stock_code", unique = true, nullable = false, length = 10)
	public String getStockCode() {
		return this.stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	@Column(name = "stock_name", length = 20)
	public String getStockName() {
		return this.stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	@Column(name = "stock_type", length = 10)
	public String getStockType() {
		return this.stockType;
	}

	public void setStockType(String stockType) {
		this.stockType = stockType;
	}

	@Column(name = "stock_market", length = 10)
	public String getStockMarket() {
		return this.stockMarket;
	}

	public void setStockMarket(String stockMarket) {
		this.stockMarket = stockMarket;
	}

}