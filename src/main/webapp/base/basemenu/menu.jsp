<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<jsp:include page="/core/tool/resources.jsp"></jsp:include>
	<script src="./menu.js"></script>

</head>

<body class="easyui-layout" style="text-align:left">

	<div data-options="region:'west',split:true" title="菜单" style="width:250px;">
		<ul id="index_menu" class="easyui-tree"  data-options="dnd:true,animate:true,
				onClick : click_menu, onStopDrag:changeMenuParent"></ul>
	</div>

	<div data-options="region:'center',border:false"  >
		<div id="toolbar" style="padding:10px 60px 10px 60px">
			<a href="#" class="easyui-linkbutton" onclick="click_addTop()">新增顶级菜单</a>
			<a href="#" class="easyui-linkbutton" onclick="click_add()">增加子菜单</a>
			<a href="#" class="easyui-linkbutton" onclick="click_save()">保存</a>
			<a href="#" class="easyui-linkbutton" onclick="click_delete()">删除</a>
		</div>
		<div style="padding:10px 60px 20px 60px">
			<form id="ff" method="post" ajax="true">
				<table cellpadding="5">
					<tr>
						<td>编码:</td>
						<td><input class="easyui-textbox" type="text" name="code" required="true"/></td>
						<td>父编码:</td>
						<td><input class="easyui-textbox" type="text" readonly="true" name="pcode"/></td>
					</tr>
					<tr>
						<td>名称:</td>
						<td><input class="easyui-textbox" type="text" name="name" required="true"/></td>
						<td>地址:</td>
						<td><input class="easyui-textbox" type="text" name="url"/></td>
					</tr>
					<tr>
						<td>排序:</td>
						<td><input class="easyui-textbox" type="text" name="sort"/></td>
						<td>图标:</td>
						<td><input class="easyui-textbox" type="text" name="picture"/></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</body>
</html>
