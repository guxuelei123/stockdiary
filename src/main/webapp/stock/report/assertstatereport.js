$(function(){
	//	getReport();
});

function getReport(){
	$.post(plat.fullUrl("/report/assertstatet/query.do"),
			$('#ff').form("getData"),
			loadData
		);
}

function loadData(data){
	$('#ff').form("load",data.queryTime);
	
	$('#container').highcharts({
            title : {
                text : '资产统计'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series : [{
	                name : '总资产',
	                data : data.allAssert,
	                tooltip: {
	                    valueDecimals: 2
	                }
	            },{
	                name : '总成本',
	                data : data.allCost,
	                tooltip: {
	                    valueDecimals: 2
	                }
            }],
            xAxis: {
            	type: 'datetime',
            	categories:data.nowTime,
                labels: {  
                    step: Math.floor(data.nowTime.length/10),   
                    formatter: function () {  
                        return Highcharts.dateFormat('%y-%m-%d', this.value);  
                    }  
                }
            } ,
            tooltip:{
                xDateFormat: '%Y-%m-%d',
                shared: true
            }
            
        });
}